﻿
//by AJ

#include "pch.h"
#include <iostream>
#include <string>
#include <stdio.h> 
#include <conio.h> 
#include <cstdlib>
#include <stdlib.h>
#include <ctime>
#include <windows.h>
using namespace std;
CONSOLE_SCREEN_BUFFER_INFO console;
char in;
void defineAscii() {
	string infAscii = R"(ASCII to Amerykański Standard Kodowania dla wymiany informacji.)"
"\nJest to siedmiobitowy system kodowania znaków. \n"
	 "Używa się go w niemal każdym urządzeniu posiadającym procesor. \n"
	"Przyporządkowuje liczbom z zakresu 0 do 127: \n"
 "litery alfabetu angielskiego, \n"
 "cyfry, \n" 
"znaki przestankowe, \n"
 "symbole, \n"
 "polecenia sterujące np. del, shift, tab. \n"
		"W systemie ascii nie występują jakiekolwiek litery tworzone przez dodanie znaku zmieniającego sposób odczytu (znaki diakrytyczne. \n";
	printf("%s\n", infAscii.c_str());
}

void showAscii() {
		string ascii = R"( 0  NUL(null))" 
			"\n 1  SOH(start of heading) \n "
			"2  STX(start of text) \n "
			"3  ETX(end of text) \n "
			"4  EOT(end of transmission) \n "
			"5  ENQ(enquiry) \n " 
			"6  ACK(acknowledge) \n " 
			"7  BEL(bell) \n " 
			"8  BS(backspace) \n " 
			"9  TAB(horizontal tab) \n " 
			"10  LF(NL line feed, new line) \n " 
			"11  VT(vertical tab) \n " 
			"12  FF(NP form feed, new page) \n " 
			"13  CR(carriage return) \n " 
			"14  SO(shift out) \n " 
			"15  SI(shift in) \n " 
			"16  DLE(data link escape) \n " 
			"17  DC1(device control 1) \n " 
			"18  DC2(device control 2) \n " 
			"19  DC3(device control 3) \n " 
			"20  DC4(device control 4) \n " 
			"21  NAK(negative acknowledge) \n " 
			"22  SYN(synchronous idle) \n " 
			"23  ETB(end of trans.block) \n " 
			"24  CAN(cancel) \n " 
			"25  EM(end of medium) \n " 
			"26  SUB(substitute) \n " 
			"27  ESC(escape) \n " 
			"28  FS(file separator) \n " 
			"29  GS(group separator) \n " 
			"30  RS(record separator) \n " 
			"31  US(unit separator) \n " 
				"32  SPACE \n ";
		cout << " Znaki w tablicy ASCII: \n =============================== \n";
		printf("%s\n", ascii.c_str());
		for (unsigned char i = 33; i < 128; i++) {
			cout << int(i)<<" "<< i<<endl;
		}
}

void getAscii() {
	unsigned char ch;
	do
	{
		cout << "Wprowadź znak z klawiatury: ";
		
		ch = _getch();
		cout << "wartość w tablicy ASCII dla " << ch << " to " << int(ch)<<endl;

		while (_kbhit()) {
			ch = _getch();
			cout << "wartość w tablicy ASCII dla " << ch << " to " << int(ch)<<endl;
		}

	} while (ch != 27); //ESC
}


void matrix(string matrixThis, bool random) {


		/* get the width of the console */
		int ret = GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &console);
		int width = 80;
		if (ret)
		{
			width = console.dwSize.X;
		}
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_INTENSITY); //intensywny zielony
		for (;;) {
			
			for (int i = 1; i < width; i++) {
				
				char cch = 'a' + rand() % i;
				srand(time(NULL));
				cout << cch;
			}
		}
}

void Menu(string menuName) {
	int s = 0;
	string m;
	setlocale(LC_CTYPE, "Polish");

	do {
		printf("%s\n", menuName.c_str());
		cout << "================== \n ";
		cout << "1. Co to jest tablica ASCII \n ";
		cout << "2. Znaki w tablicy ASCII \n ";
		cout << "3. Wartość dla podanego znaku z tablicy \n ";
		cout << "4. Kod Matrix dla wybranych znaków \n ";
		cout << "5. Kod Matrix \n ";
		cout << "6. Zakończ program \n ";
		cout << "================== \n ";
		in = _getch();
		system("cls");
		switch (in) {
		case '1':
			defineAscii();
			break;
		case '2':
			showAscii();
			break;
		case '3':
			cout << "Kliknij ESC, aby przerwać \n";
			getAscii();
			break;
		case '4':
			cout << "Podaj liczby do matrix'a: ";
			cin >> m;
			matrix(m, 0);
			break;
		case '5':
			matrix(m, 1);
			break;
		case '6':
			exit(0);
			break;
		default: "Spróbuj ponownie";
		}

		_getch();
	} while (s == 0);
	
	
}

int main()
{
	Menu("MENU GŁÓWNE");
	return 0;
}

